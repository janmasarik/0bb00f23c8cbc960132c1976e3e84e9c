### Keybase proof

I hereby claim:

  * I am janmasarik on github.
  * I am sl4ve (https://keybase.io/sl4ve) on keybase.
  * I have a public key ASDtny5-gzswBO2UP71K3YNIrwomIrthIfX9PVp9iJH09go

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120ed9f2e7e833b3004ed943fbd4add8348af0a2622bb6121f5fd3d5a7d8891f4f60a",
      "host": "keybase.io",
      "kid": "0120ed9f2e7e833b3004ed943fbd4add8348af0a2622bb6121f5fd3d5a7d8891f4f60a",
      "uid": "a661fc11022e0db4a5c11e82fd03d419",
      "username": "sl4ve"
    },
    "merkle_root": {
      "ctime": 1508234977,
      "hash": "1d7bcbbf3c62075e6ea041d5e128d1a75f3e8fc475dd2bcd9ca2aa14155b3f7268cb9a9ba57484986a47f12107daaf0501f1fc652355f2a968af132b7fa74276",
      "hash_meta": "01abe7b0a729c164ec5adba24d328ed06628dfcf61479b6cb5875f3af0d3788c",
      "seqno": 1589966
    },
    "service": {
      "name": "github",
      "username": "janmasarik"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.33"
  },
  "ctime": 1508235337,
  "expire_in": 504576000,
  "prev": "ca3f988b5221f719b81ae0a39f9660592f187deb61c8d2e7cbfdf9de7ad3bb02",
  "seqno": 13,
  "tag": "signature"
}
```

with the key [ASDtny5-gzswBO2UP71K3YNIrwomIrthIfX9PVp9iJH09go](https://keybase.io/sl4ve), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg7Z8ufoM7MATtlD+9St2DSK8KJiK7YSH1/T1afYiR9PYKp3BheWxvYWTFAzx7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwZWQ5ZjJlN2U4MzNiMzAwNGVkOTQzZmJkNGFkZDgzNDhhZjBhMjYyMmJiNjEyMWY1ZmQzZDVhN2Q4ODkxZjRmNjBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZWQ5ZjJlN2U4MzNiMzAwNGVkOTQzZmJkNGFkZDgzNDhhZjBhMjYyMmJiNjEyMWY1ZmQzZDVhN2Q4ODkxZjRmNjBhIiwidWlkIjoiYTY2MWZjMTEwMjJlMGRiNGE1YzExZTgyZmQwM2Q0MTkiLCJ1c2VybmFtZSI6InNsNHZlIn0sIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNTA4MjM0OTc3LCJoYXNoIjoiMWQ3YmNiYmYzYzYyMDc1ZTZlYTA0MWQ1ZTEyOGQxYTc1ZjNlOGZjNDc1ZGQyYmNkOWNhMmFhMTQxNTViM2Y3MjY4Y2I5YTliYTU3NDg0OTg2YTQ3ZjEyMTA3ZGFhZjA1MDFmMWZjNjUyMzU1ZjJhOTY4YWYxMzJiN2ZhNzQyNzYiLCJoYXNoX21ldGEiOiIwMWFiZTdiMGE3MjljMTY0ZWM1YWRiYTI0ZDMyOGVkMDY2MjhkZmNmNjE0NzliNmNiNTg3NWYzYWYwZDM3ODhjIiwic2Vxbm8iOjE1ODk5NjZ9LCJzZXJ2aWNlIjp7Im5hbWUiOiJnaXRodWIiLCJ1c2VybmFtZSI6Imphbm1hc2FyaWsifSwidHlwZSI6IndlYl9zZXJ2aWNlX2JpbmRpbmciLCJ2ZXJzaW9uIjoxfSwiY2xpZW50Ijp7Im5hbWUiOiJrZXliYXNlLmlvIGdvIGNsaWVudCIsInZlcnNpb24iOiIxLjAuMzMifSwiY3RpbWUiOjE1MDgyMzUzMzcsImV4cGlyZV9pbiI6NTA0NTc2MDAwLCJwcmV2IjoiY2EzZjk4OGI1MjIxZjcxOWI4MWFlMGEzOWY5NjYwNTkyZjE4N2RlYjYxYzhkMmU3Y2JmZGY5ZGU3YWQzYmIwMiIsInNlcW5vIjoxMywidGFnIjoic2lnbmF0dXJlIn2jc2lnxEDKiD+LZgb/17yow3ZKaJI0cHyjn8UG8JMRpmziUC5TJK9HSCxq2nLxCrz+2nJdsx4rPUwkyjtVj6zxyA1ZUOUHqHNpZ190eXBlIKRoYXNogqR0eXBlCKV2YWx1ZcQgJvmgiksTHgBRCmaVCrEi30GlgQzlG6bJlZAf8cecxj+jdGFnzQICp3ZlcnNpb24B

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/sl4ve

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id sl4ve
```